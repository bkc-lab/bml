def parse_swagger():
    sample_swagger = {
        "host": "localhost:7999",
        "basePath": "/",
        "swagger": "2.0",
        "schemes": [
            "http"
        ],
        "info": {
            "version": "1",
            "license": {
                "name": "Apache 2.0",
                "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
            },
            "contact": {
                "email": "nguyenquangthang1997@gmail.com"
            },
            "title": "Query data Api",
            "description": "identification API"
        },
        "tags": [
            {
                "name": "user"
            }
        ],
        "paths": {
            "/query": {
                "post": {
                    "consumes": [
                        "json"
                    ],
                    "produces": [
                        "application/json"
                    ],
                    "summary": "query transformed data",
                    "description": "Query transformed data",
                    "operationId": "create",
                    "responses": {
                        "201": {
                            "description": "Transformed data in blockchain",
                            "schema": {
                                "type": "object",
                                "properties": {
                                    "data": {
                                        "type": "array",
                                        "items": {
                                            "type": "object",
                                            "properties": {
                                                "attribute1": {
                                                    "type": "string"
                                                },
                                                "attribute2": {
                                                    "type": "string"
                                                },
                                                "attribute3": {
                                                    "type": "string"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    "tags": [
                        "user"
                    ],
                    "parameters": [
                        {
                            "in": "header",
                            "name": "Authorization",
                            "type": "string",
                            "description": "Authorization to identify user"
                        },
                        {
                            "in": "body",
                            "name": "query",
                            "description": "Query description",
                            "schema": {
                                "type": "object",
                                "properties": {
                                    "identifier": {
                                        "type": "string"
                                    },
                                    "filter": {
                                        "type": "array",
                                        "items": {
                                            "type": "string",
                                            "example": "id:int(id)==1"
                                        }
                                    }
                                }
                            }
                        }
                    ]
                }
            }
        }
    }
    return sample_swagger
