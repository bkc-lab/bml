FROM python:3.6 as final
ARG requirements=requirements.txt


WORKDIR /bml
COPY . /bml


RUN pip install -r $requirements

EXPOSE 7999
